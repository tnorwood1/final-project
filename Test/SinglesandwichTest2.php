<?php

class SinglesandwichTest extends PHPUnit_Framework_TestCase {

	private $CI;
        /**
         * Function that gets reference to the Codeigniter super object to return all classes
         */
	public function setUp() {
		$this->CI = &get_instance();
	}
        /**
         * Tests function to see that it returns an array when passed 
         * an ID that is in the database.
         * 
         */
	public function testIndexReturnsArrayWhenIDisInDatabase() {
                $ID = 'ID = 6';
		$this->CI->load->model('singlesandwich');
		$query = $this->CI->singlesandwich->index($ID);
		$this->assertInternalType('array', $query);
	}
        /**
         * Tests function to see that it returns an array when passed
         * sub name that is in database.
         */
        public function testIndexReturnsArrayWhenSubisInDatabase() {
                $subs = 'Sandwich = "The Sissy"';
		$this->CI->load->model('singlesandwich');
		$query = $this->CI->singlesandwich->index($subs);
		$this->assertInternalType('array', $query);
	}
        /**
         * Tests function to see that it returns an array
         * that is greater than zero
         * 
         */
	public function testIndex() {
		$this->CI->load->model('subsmodel');
		$query = $this->CI->subsmodel->getSubs();
		$this->assertInternalType('array', $query);
	}
}
?>
