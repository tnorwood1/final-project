<?php
//Form controller 
class Form extends CI_Controller {
        /**
         * Function used for form
         * 
         */	function index()
	{       $this->load->helper(array('form','url'));
    
    // ensure user is signed in
    if ( $this->session->userdata('login') == FALSE ) {
	 $this->session->set_flashdata('errors', 'You must be logged in to view pages!');	
     redirect( $this->input->post('redirect') );    // no session established, kick back to login page
    }

                //load url helper for form
                $this->load->helper(array('form', 'url'));

		//load form validation and values
                $this->load->library('form_validation');
                $this->form_validation->set_rules('Sissy', 'Sissy', 'min_length[1]|numeric');
		$this->form_validation->set_rules('Corleone', 'Corleone', 'min_length[1]|numeric');
                $this->form_validation->set_rules('Mediterranean', 'Mediterranean', 'min_length[1]|numeric');
                $this->form_validation->set_rules('Greasy', 'Greasy', 'min_length[1]|numeric');
                $this->form_validation->set_rules('Plain', 'Plain', 'min_length[1]|numeric');
                $this->form_validation->set_rules('Porker', 'Porker', 'min_length[1]|numeric');
                $this->form_validation->set_rules('Insanity', 'Insanity', 'min_length[1]|numeric');
                $this->form_validation->set_rules('Fname', 'Fname', 'required|min_length[4]|max_length[20]|alpha_dash_space');
                $this->form_validation->set_rules('Lname', 'Lname', 'required|min_length[4]|max_length[20]|alpha_dash_space');
                $this->form_validation->set_rules('Street', 'Street', 'required|min_length[4]|alpha_dash_space');
                $this->form_validation->set_rules('Street2', 'Street2', 'min_length[2]|alpha_dash_space');
                $this->form_validation->set_rules('City', 'City', 'required|min_length[4]|alpha_dash_space');
                $this->form_validation->set_rules('State', 'State', 'required|exact_length[2]|alpha');
                $this->form_validation->set_rules('Zip', 'Zip', 'required|exact_length[5]|numeric');
                $this->form_validation->set_rules('Payment', 'Payment', 'required');
                
                 
                //if else statement to load form validation errors or view
                if ($this->form_validation->run() == FALSE)
		{
		     $this->load->view('myform');
                   
		}
		else
		{   $var = $this->input->post('Sissy'); 
                 
                        //manipulation code to create cost values		
                        $subtotal['subtotal'] = ($var* 4.99) + ($this->input->post('Corleone')* 5.50)+($this->input->post('Mediterranean')* 6.99)+
                                                ($this->input->post('Greasy')* 8.00)+($this->input->post('Plain')* 5.50)+($this->input->post('Porker')* 7.99)+
                                                ($this->input->post('Insanity')* 9.99);
                        
                        if ($this->input->post('Payment') == 2)
                            {
                            $subtotal['cc'] = $subtotal['subtotal'] * .015;
                            }
                        else{
                            $subtotal['cc'] = 0;
                            }
                            $subtotal['taxes'] = $subtotal['subtotal'] * .075;
                            $subtotal['total'] = $subtotal['subtotal'] +  $subtotal['cc'] + $subtotal['taxes'];
             
                        $this->load->view('formsuccess', $subtotal);                                   
                 
                 $this->load->model('subsmodel');
                 // array data that is passed to the setSubs function                     
                $data=array(
                 'Sissy'=>        $this->input->post('Sissy'),
                 'Corleone'=>     $this->input->post('Corleone'),
                 'Mediterranean'=>$this->input->post('Mediterranean'),
                 'Greasy'=>       $this->input->post('Greasy'),
                 'Plain'=>        $this->input->post('Plain'),
                 'Porker'=>       $this->input->post('Porker'),
                 'Insanity'=>     $this->input->post('Insanity'),
                 'Fname'=>        $this->input->post('Fname'),
                 'Lname'=>        $this->input->post('Lname'),
                 'Street'=>       $this->input->post('Street'),   
                 'Street'=>       $this->input->post('Street'),
                 'Street2'=>      $this->input->post('Street2'),
                 'City'=>         $this->input->post('City'),
                 'State'=>        $this->input->post('State'),
                 'Zip'=>          $this->input->post('Zip'),
                 'Payment'=>      $this->input->post('Payment'),
                 'Newsletter'=>   $this->input->post('Newsletter'));                
                  $this->subsmodel->setSubs($data);     
                 }
        }
        
        
}
?>