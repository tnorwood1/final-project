<?php
class Login extends CI_controller{
 
function index(){

  		$this->load->helper(array('form', 'url'));

		//load form validation and values
                $this->load->library('form_validation');
                $this->form_validation->set_rules('Username', 'Username', 'required|max_length[12]|alpha_numeric');
		$this->form_validation->set_rules('Password', 'Password', 'required|max_length[12]|alpha_numeric');
     
                //if else statement to load form validation errors or
                // view
                if ($this->form_validation->run() == FALSE)
		{
		     $this->load->view('login_view');
		}
		else
		{  
                return false;
                }

   
    }
}
?>
