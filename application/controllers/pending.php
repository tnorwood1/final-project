<?php
class pending extends CI_Controller{
     function index() {

    $this->load->helper(array('form','url'));
    
    // ensure user is signed in before they can view pending page
    if ( $this->session->userdata('login') == FALSE ) 
        {
	 $this->session->set_flashdata('errors', 'You must be logged in to view pages!');	
         redirect( $this->input->post('redirect') );    // no session established, return to login page
        }

               
               $this->load->model('subsmodel');
               // if else statement to pass pending value to column
               if ($this->input->post('Order_Status') == false)
               {
                $data=array(
               'Order_Status'=> '1' );
               }
               else{
               $data=array(
               'Order_Status'=> $this->input->post('Order_Status'));
               }
               //Load model and load view of pending orders
               $this->subsmodel->setOrderstatus($data);       
               $this->load->model('subsmodel');
               $datas['query'] = $this->subsmodel->getPorders();
               $this->load->view('pendingorders', $datas);
               }
           /**
            * Function to pass values from pending to process or reload view
            */
     function processed()
               {
               $this->load->model('subsmodel');              
               if($this->input->post('Order_Status') == 1){
                $data=array(
               'Order_Status'=> '1' );
                $this->load->view('myform');
               }
               else
               {
               $data=array(
               'Order_Status'=> $this->input->post('Order_Status'));
               $this->subsmodel->setOrderstatus($data);       
               $mess['thanks'] = "Thanks for your order!";
               $this->load->view('complete',$mess);
               
               }
             }
           }
?>
