<?php 
class Subs extends CI_controller{
 /**
 * Function used for subs
 */
function index(){
    
    //load helper to print link for form.php
  $this->load->helper('url');
  
    // /load model and call the class name from model
  
   $this->load->model('subsmodel');
   
   // query to get values from database
   $data['query'] = $this->subsmodel->getSubs();
   
   
//   This code was used to hash passwords in database ----------------
//   $data['query2'] = $this->subsmodel->getPw();
//   foreach($data['query2'] as $e){
//        $a = $e->ID;  $b = $e->Password;
//        $this->load->library('encrypt');
//        $hash = $this->encrypt->sha1($b);
//        $query = 'update Users set Password = ? where ID = ?';
//        $this->db->query($query, array($hash,$a));
//     ----------------------------------------------------------------
        $this->load->view('subs_view', $data);
       
   }

}
?>