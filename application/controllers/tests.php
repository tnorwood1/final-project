<?php
class Tests extends CI_controller{
    /**
     * Function to get data from database for tests
     */
    function index(){
        $this->load->model('singlesandwich');
        $ID = 'ID = 10';
        $subs = 'Sandwich = "The Sissy"';
        $data['query']  = $this->singlesandwich->index($ID);
        $data['quer'] = $this->singlesandwich->index($subs);
        $this->load->model('subsmodel');
        $data['que'] = $this->subsmodel->getSubs();

     
        //load the unit test function
        $this->load->library('unit_test');       
        $this->unit->run($this->singlesandwich->index($ID), 'is_array', 'Single_Sandwich_method_returns_array_if_ID_is_in_database!');
        $this->unit->run($this->singlesandwich->index($subs), 'is_array', 'Single_Sandwich_method_checks_if_Sandwich_is_in_database!');
        $this->unit->run($this->subsmodel->getSubs(), 'is_array', 'Get_Subs_method_is_checked_to _see_it_returns_an_array_greater_than_0!');
        $data['testing'] = $this->unit->report();
        $this->load->view('test_view', $data);

    }
}
        
?>

