<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class VerifyLogin extends CI_Controller {
	 function index()
	 {
	   //This method will have the credentials validation
	   $this->load->library('form_validation');
	 
	   $this->form_validation->set_rules('Username', 'Username', 'trim|required|xss_clean|callback_username_check');
	   $this->form_validation->set_rules('Password', 'Password', 'trim|required|xss_clean|callback_password_check');
	       

	   if($this->form_validation->run() == FALSE)
	   {
	     //Field validation failed.  User redirected to login page
	     $this->load->view('login_view');
             
	   }
	   else
	   {
	     //Go to private area
	     redirect('subs', 'refresh');
	   }
	 
           }/**
            * Function used to check if username and password combination is in database
            * @return boolean
            */
         	public function username_check()              
	        {       
                $this->load->model('loginmodel');
		if ($this->loginmodel->getUsers() === false)
		{
			$this->form_validation->set_message('username_check', 'The username and password combination is not valid');
			return FALSE;
		}
		else
		{
			return TRUE;
                      
		}

	}
            
        }
         ?>
