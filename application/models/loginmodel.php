<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    /**
     * function used to get values from Subs table
     * @return array
     */
class Loginmodel extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function getUsers(){
        $this->load->library('encrypt');
        $hash = $this->encrypt->sha1($this->input->post('Password'));
        
        // grab user input
        $username = $this->security->xss_clean($this->input->post('Username'));
        $password = $hash;
        
        // Prep the query
        $this->db->where('Username', $username);
        $this->db->where('Password', $password);
        
        // Run the query
        $query = $this->db->get('Users');
        // Let's check if there are any results
        if($query->num_rows == 1)
        {
            // If there is a user, then create session data
            $row = $query->row();
            $data = array(
                    'ID' => $row->ID,
                    'Username' => $row->Username,
                    'validated' => true
                    );
            $this->session->set_userdata('login',$data);
            return true;
        }
        // If the previous process did not validate
        // then return false.
        return false;
    }

}
?>
