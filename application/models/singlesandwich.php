<?php

class Singlesandwich extends CI_Model{
/**
 * 
 * @param type $value
 * @return array
 */
    public function index($value){
        $this->load->database();
        $this->db->where($value);
        $query = $this->db->get('Subs');     
        if ($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
        
    }
}
?>
