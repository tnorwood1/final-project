<?php 

class Subsmodel extends CI_Model{
    /**
     * function used to get values from Subs table
     * @return array
     */
    public function getSubs()
        {
         $this->load->database();
         $S = $this->db->get('Subs');
         if ($S->num_rows() > 1){
         return $S->result();  
         }
         else {
             return false;
         }
        }/**
         * Function to get order status from database
         * @return boolean
         */
         public function getPorders()
        {
         $this->load->database();
         $this->db->where('Order_Status','Pending');
         $S = $this->db->get('Orders');
         if ($S->num_rows() > 1)
         {
         return $S->result();  
         }
         else 
          {
             return false;
          }
         }
            /**
             * Function used to get ID and Password from database
             * @return $object
             */
         public function getPw(){
         $this->load->database();
         $this->db->select('ID');
         $this->db->select('Password');
         $S = $this->db->get('Users');
         return $S->result();
         }

        /**
         * function used to pass values to Orders table 
         * @param type $data
         */
    public function setSubs($data){
         $this->load->database();
         $order_info= $data['Sissy'];
         $order_info= $data['Corleone'];
         $order_info= $data['Mediterranean'];
         $order_info= $data['Greasy'];
         $order_info= $data['Plain'];
         $order_info= $data['Porker'];
         $order_info= $data['Insanity'];
         $order_info= $data['Fname'];
         $order_info= $data['Lname'];
         $order_info= $data['Street'];
         $order_info= $data['Street2'];
         $order_info= $data['City'];
         $order_info= $data['State'];
         $order_info= $data['Zip'];
         $order_info= $data['Payment'];
         $order_info= $data['Newsletter'];
         $this->db->insert('Orders',$data);
     }
     
     /**
      * Function used to set the order status
      * @param type $data
      */
       public function setOrderstatus($data){
         $this->load->database();
         $query = $this->db->query('SELECT LAST_INSERT_ID()');
         $row = $query->row_array();
         $lastInsertId = $row['LAST_INSERT_ID()'];
         $this->db->where('ID', $lastInsertId);
         $this->db->update('Orders',$data);
     }
      
    

               
}
?>