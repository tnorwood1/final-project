<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Order Form</title>
    </head>
<body>

<h3><?php echo $this->input->post('Fname').' '.$this->input->post('Lname');?>, review your order before processing it!</h3>

 <h4> You ordered:</h4>
    <p><?php echo "Sissy Sandwich:"         . ' ' . $this->input->post('Sissy');?></p>
    <p><?php echo "Corleone Sandwich:"      . ' ' . $this->input->post('Corleone');?></p>
    <p><?php echo "Mediterranean Sandwich:" . ' ' . $this->input->post('Mediterranean');?></p>
    <p><?php echo "Greasy Sandwich:"        . ' ' . $this->input->post('Greasy');?></p>
    <p><?php echo "Plain Sandwich:"         . ' ' . $this->input->post('Plain');?></p>
    <p><?php echo "Porker Sandwich:"        . ' ' . $this->input->post('Porker');?></p>
    <p><?php echo "Insanity Sandwich:"      . ' ' . $this->input->post('Insanity');?></p>
    <h4>Your payment is:</h4>
    <p><?php echo "Subtotal:"        . ' ' . number_format($subtotal,2);?></p> 
    <p><?php echo "Credit card fee:" . ' ' . number_format($cc,2);?></p> 
    <p><?php echo "Taxes:"           . ' ' . number_format($taxes,2);?></p> 
    <p><?php echo "Total:"           . ' ' . number_format($total,2);?></p>  


 <?php echo form_open('pending'); ?>

    <button type='submit'>View Pending orders!</button>         
</form> 
<p><?php echo anchor('form', 'Order incorrect, replace it!'); ?></p>
</body>
</html>

