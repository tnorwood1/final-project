<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <title>Order Form</title>
    </head>
<body>
   <h1>Order</h1>   
    <?php echo validation_errors(); ?>
    <?php echo form_open('form'); ?>
   
            <p>How many subs of each would you like?</p>
            
            <p>
                <label>The Sissy</label>
                <input type='text' name='Sissy'    value="<?php echo set_value('Sissy', 0); ?>" />
            </p>           
             <p>
                <label>The Corleone</label>
                <input type='text' name='Corleone'  value="<?php echo set_value('Corleone', 0); ?>"/>
            </p>
             <p>
                <label>The Mediterranean</label>
                <input type='text' name='Mediterranean'   value="<?php echo set_value('Mediterranean', 0); ?>"/>
            </p>
             <p>
                <label>The Greasy Pizza</label>
                <input type='text' name='Greasy'    value="<?php echo set_value('Greasy',0); ?>"/>
            </p>
             <p>
                <label>The Plain and Simple</label>
                <input type='text' name='Plain'    value="<?php echo set_value('Plain', 0); ?>" />
            </p>
             <p>
                <label>The Porker</label>
                <input type='text' name='Porker'    value="<?php echo set_value('Porker', 0); ?>"/>
            </p>
             <p>
                <label>The Sub of Insanity</label>
                <input type='text' name='Insanity' value="<?php echo set_value('Insanity', 0); ?>"/>
             </p>
            <h2> Customer Info:</h2>
             <p>
                <label>Name:</label>
                <input type='text' name='Fname' placeholder='First name'  value="<?php echo set_value('Fname'); ?>"  />
                <input type='text' name='Lname' placeholder='Last name' value="<?php echo set_value('Lname'); ?>" />
            </p>
            <p>
                <label>Address:</label>
                <input type='text' name='Street'  placeholder='Street'  value="<?php echo set_value('Street'); ?>"  />
                <input type='text' name='Street2' placeholder='Street2' value="<?php echo set_value('Street2'); ?>" />
            </p>
            <p>
                <input type='text' name='City'  placeholder='City'  value="<?php echo set_value('City'); ?>" />
                <input type='text' name='State' placeholder='State' value="<?php echo set_value('State'); ?>"/>
                <input type='text' name='Zip'   placeholder='Zip'   value="<?php echo set_value('Zip'); ?>"/>
            </p>
            <p>
                <label>Payment Type:</label>
                <input type="radio" name="Payment" value="1" <?php echo set_radio('Payment','1'); ?>/> Cash
                <input type="radio" name="Payment" value="2" <?php echo set_radio('Payment','2'); ?>/>Credit
            </p>            
            <p>
                <label>Sign-up for newsletter:(Leave box unchecked if no)</label>
                <input type='checkbox' name="Newsletter" id="yes" value="<?php echo set_value('Newsletter','1');?>"/>
                <label for="yes">yes</label>              
            </p>         
            <p>
                <button type='submit'>Submit This!</button>
            </p>          
        </form>       
    </body>
</html>
